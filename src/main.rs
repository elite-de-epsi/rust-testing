

fn prints_and_returns_10(a: i32) -> i32 {
    println!("I got the value {}", a);
    10
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn this_test_will_pass() {
        let value = prints_and_returns_10(4);
        assert_eq!(10, value);
    }
}


//Etant donne une  machine à café
// QUAND on scanne une carte credit
// ALORS retire 40 de la carte
// SI PAIEMENT VALIDé
// Alors café servi


//Etant donne une  machine à café
// QUAND un café est servis
// ALORS un gobelet est retiré de la machine


//ETANT DONNE une machine ET un appui sur le bouton sucre QUAND on insère 40cts ALORS un café coule ET une dose de sucre est consommée


struct Machine {
    funds: i32,
    sugar: i32,
    resource: i32,
    water: i32,

}

struct Card {
     card: bool
}

impl Machine {
    fn new() -> Self {
        Self { funds: 0, sugar: 10,resource: 0, water: 10 }
    }

    fn insert_funds(&mut self, amount: i32) {
        self.funds += amount;
    }

    fn press_sugar_button(&mut self) {
        if self.sugar > 0 {
            self.sugar -= 1;
        }
    }

    fn dispense_coffee(&self) -> bool {
        self.funds >= 40
    }

    fn refill_resource(&mut self) {
        self.resource += 1;
    }

    fn make_coffee(&mut self) -> bool {
        if self.water < 2 {
            return false;
        }

        self.water -= 2;
        true
    }
}

impl Card {
    fn new() -> Self {
        Self { card: false  }
    }
}



fn test_coffee_with_sugar() {

    let mut machine = Machine::new();
//sugar

    // // Insertion de 40 cts
    // machine.insert_funds(40);
    //
    // // Appui sur le bouton sucre
    // machine.press_sugar_button();
    //
    // // Test de l'énoncé
    // if machine.dispense_coffee() {
    //     println!("Un café coule et une dose de sucre est consommée.");
    // } else {
    //     println!("La machine ne peut pas distribuer de café.");
    // }

//restock
    let initial_resource = machine.resource;

    // Appui sur le bouton de réapprovisionnement
    machine.refill_resource();

    // Test de l'énoncé
    if machine.resource < 10 {
        println!("Le stock n'a pas atteint 10 unités. Sa valeur est maintenant {}.", machine.resource);
    } else {
        println!("Le stock a atteint ou dépassé 10 unités. Sa valeur est maintenant {}.", machine.resource);
    }

}
//ETANT DONNE une machine ET un scan cb QUAND choisis un café et qu’on paye par cb ALORS un cafe coule ET 2 doses d'eau sont consommées
fn test_payment_by_cb() {
    let mut machine = Machine::new();

    let success = machine.make_coffee();
    if success {
        println!("Un café a coulé et 2 doses d'eau ont été consommées. Il reste {} doses d'eau.", machine.water);
    } else {
        println!("Un café n'a pas pu être servi car il n'y avait pas assez d'eau. Il reste {} doses d'eau.", machine.water);
    }
}

//ETANT DONNE une machine QUAND le stock d’eau est pret d’etre vide ALORS un on réapprovisionne l’eau


fn main() {

    let mut card = Card::new();

    // test_coffee_with_sugar();

    if( card.card == true){
        test_payment_by_cb();
    }
    else{
        println!("pas de payement par carte");

    }


}

